var gulp        = require('gulp');

module.exports = function(buildProperties, callback) {
    return gulp.src(buildProperties.appFiles.assetFiles)
        .pipe(gulp.dest(buildProperties.expressRoot))
        .pipe(gulp.dest(buildProperties.publicDir));
};
