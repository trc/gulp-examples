#Gulp Examples

This repository contains a series of examples on the basic use of gulp for building an HTML & JavaScript, or HTML and AngularJS project.

This is not meant to be an exhaustive list of examples for everything that can be done with gulp and the structure we've used to organize our more complicated examples may not be to everyone's tastes (though it's working very well for us).

That being said, I hope that these examples are instructive for those learning how to use Gulp and that our experience in structuring a fairly large build (see examples 8 & 9, where our gulp actions are loaded as separate files from gulp/actions/*.js) prove helpful to you in your project. 

##To Run the Examples

Make sure you've installed node.js, and install Gulp globally by running 'npm install -g gulp'.

After cloning this repository to your filesystem, cd into the directory for the example you'd like to run and execute the following:

    1. npm install
    2. bower install (only needed for examples 6 - 9)
    3. gulp
