var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

module.exports = function(buildProperties, callback) {
    return gulp.src(buildProperties.appFiles.scssFiles)
        .pipe(plugins.concat('./css/app.scss'))
        .pipe(plugins.streamify(plugins.size({ showFiles: true })))
        .pipe(plugins.streamify(plugins.sass()))
        .pipe(plugins.rename('./css/app.css'))
        .pipe(gulp.dest(buildProperties.expressRoot))
        .pipe(plugins.minifyCss())
        .pipe(plugins.streamify(plugins.rev()))
        .pipe(plugins.streamify(plugins.size({ showFiles: true })))
        .pipe(gulp.dest(buildProperties.publicDir))
        .on('error', plugins.util.log);
};