module.exports = function(index, intervalTime, callback) {
    console.log("\t\t\tdo-something-0" + index + " starting...");
    var iterations = 0;
    var interval = setInterval(function() {
        iterations++;
        if(iterations >= 10)
        {
            clearInterval(interval);
            if(callback) callback();
        }
        else
        {
            console.log("\t\t\tdo-something-0" + index + " doing something...");
        }
    }, intervalTime);
};
