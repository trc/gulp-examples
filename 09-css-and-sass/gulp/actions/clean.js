var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

module.exports = function(callback) {
 return gulp.src("./build/*", {read: false})
     .pipe(plugins.rimraf({force: true}))
     .on('error', plugins.util.log);
}
