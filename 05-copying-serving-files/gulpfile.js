'use strict';

var express             = require('express'),
    gulp                = require('gulp'),
    plugins             = require('gulp-load-plugins')({ lazy: true}),
    runSequence         = require('run-sequence');

var buildProperties = {
    expressPort : 4005,
    expressRoot     : require('path').resolve('./build/express-tmp'),
    publicDir       : require('path').resolve('./build/www')
}

gulp.task('default', function(callback) {

    runSequence('clean', ['copy-assets', 'copy-html'], 'run-express', function()
    {
        console.log("\r\n\r\n\t\tHello from Gulp!\r\n");
        console.log("\r\n\t\tWe're now serving up our application on localhost:" + buildProperties.expressPort + "...\r\n\r\n");
    });
});

gulp.task('clean', function(callback){
    return gulp.src("./build/*", {read: false})
            .pipe(plugins.rimraf({force: true}))
            .on('error', plugins.util.log);
});

gulp.task('copy-assets', function(callback) {
    return gulp.src(['./app/assets/**/*', './app/assets/*'])
            .pipe(gulp.dest(buildProperties.expressRoot))
            .pipe(gulp.dest(buildProperties.publicDir));
});

gulp.task('copy-html', function(callback) {
    return gulp.src('./app/index.html')
            .pipe(gulp.dest(buildProperties.expressRoot))
            .pipe(gulp.dest(buildProperties.publicDir));
});

gulp.task('run-express', function(callback) {

    var app = express();
    app.use(express.static(buildProperties.expressRoot));
    app.listen(buildProperties.expressPort);
    console.log('Server running at http://localhost:'+buildProperties.expressPort);

    if(callback) callback();

});