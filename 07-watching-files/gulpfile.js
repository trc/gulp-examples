'use strict';

var connectLr           = require('connect-livereload'),
    express             = require('express'),
    gulp                = require('gulp'),
    lrServer          = require('tiny-lr')(),
    plugins             = require('gulp-load-plugins')({ lazy: true}),
    runSequence         = require('run-sequence');

var buildProperties = {
    expressPort         : 4007,
    expressRoot         : require('path').resolve('./build/express-tmp'),
    livereloadPort      : 35729,
    publicDir           : require('path').resolve('./build/www')

}

gulp.task('default', function(callback) {

    runSequence('clean', 'build-vendor-js', 'build-app-js', ['copy-assets', 'copy-html'], 'run-express', function()
    {
        console.log("\r\n\r\n\t\tHello from Gulp!\r\n");
        console.log("\r\n\t\tWe're now serving up an AngularJS application on localhost:" + buildProperties.expressPort + "...\r\n\r\n");
    });

    gulp.watch('app/index.html', function(){
       runSequence('copy-html', 'notifyReload');
    });

    gulp.watch('app/assets/*', function(){
        runSequence('copy-assets', 'notifyReload');
    });

    gulp.watch('app/assets/**/*', function(){
        runSequence('copy-assets', 'notifyReload');
    });

    gulp.watch('app/js/*.js', function(){
        runSequence('build-app-js', 'notifyReload');
    });
});

gulp.task('clean', function(callback){
    return gulp.src("./build/*", {read: false})
            .pipe(plugins.rimraf({force: true}))
            .on('error', plugins.util.log);
});

gulp.task('copy-assets', function(callback) {
    return gulp.src(['./app/assets/**/*', './app/assets/*'])
            .pipe(gulp.dest(buildProperties.expressRoot))
            .pipe(gulp.dest(buildProperties.publicDir));
});

gulp.task('copy-html', function(callback) {
    return gulp.src('./app/index.html')
            .pipe(gulp.dest(buildProperties.expressRoot))
            .pipe(plugins.minifyHtml({
                empty: true,
                spare: true,
                quotes: true
            }))
            .pipe(gulp.dest(buildProperties.publicDir));
});

gulp.task('build-vendor-js', function(callback){
    return gulp.src(['./bower_components/angular/angular.js', './bower_components/angular-bootstrap/ui-bootstrap-tpls.js'])
        .pipe(plugins.concat('js/vendor-concat.js'))
        .pipe(gulp.dest(buildProperties.expressRoot))
        .pipe(plugins.streamify(plugins.uglify({ mangle: false })))
        .pipe(plugins.rev())
        .pipe(plugins.rename({ suffix: 'min' }))
        .pipe(gulp.dest(buildProperties.publicDir));
});

gulp.task('build-app-js', function(callback){
    return gulp.src(['./app/js/*.js', './app/js/**/*.js'])
        .pipe(plugins.concat('js/app.js'))
        .pipe(gulp.dest(buildProperties.expressRoot))
        .pipe(plugins.streamify(plugins.uglify({ mangle: false })))
        .pipe(plugins.rev())
        .pipe(plugins.rename({ suffix: 'min' }))
        .pipe(gulp.dest(buildProperties.publicDir));
});

gulp.task('run-express', function(callback) {

    var app = express();
    app.use(connectLr());
    app.use(express.static(buildProperties.expressRoot));
    app.listen(buildProperties.expressPort);
    console.log('Server running at http://localhost:'+buildProperties.expressPort);

    lrServer.listen(buildProperties.livereloadPort, function(err) {
        if (err) {
            return console.log(err);
        }
    });
});


gulp.task('notifyReload', function(callback)
{
    lrServer.changed({ body: { files: ['index.html'] } });
    if(callback) callback();
});