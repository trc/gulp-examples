exports.cssFiles = [

];

exports.htmlFiles = [
   './app/index.html'
];

exports.jsFiles = [
    './app/js/*.js',
    './app/js/**/*.js'
];

exports.assetFiles = [
    './app/assets/**/*',
    './app/assets/*'
];
