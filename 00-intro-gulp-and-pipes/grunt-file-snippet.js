// Generated on 2013-07-19 using generator-angular 0.3.0
'use strict';
var LIVERELOAD_PORT = 35729;
var LIVERELOAD_APP_PORT = 35727;

var DEV_PORT = 9000;
var TEST_PORT = 9004;
var FAKE_APP_PORT = 9002;
var DIST_PORT = 9003;

var lrSnippet = require('connect-livereload')({ port: LIVERELOAD_PORT });
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

var moment = require('moment');
var util = require('util');
var _ = require('lodash');

module.exports = function (grunt) {
    require('time-grunt')(grunt);

    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.loadNpmTasks('grunt-includes');
    grunt.loadNpmTasks('grunt-prompt');
    grunt.loadNpmTasks('grunt-ssh');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-notify');

    // configurable paths
    var yeomanConfig = {
        app: 'app',
        dist: 'dist'
    };

    try {
        yeomanConfig.app = require('./bower.json').appPath || yeomanConfig.app;
    } catch (e) {
    }
    // get dev port form parameter

    DEV_PORT = grunt.option('port') || DEV_PORT;

    grunt.initConfig({
        yeoman: yeomanConfig,
        env: {
            dev: {
                PHANTOMJS_BIN: 'C:\\Code\\EntryPoint-Ux\\node_modules\\phantomjs\\lib\\phantom\\phantomjs.exe'
            }
        },
        match: {
            files: {
                // check both the scripts folder and views folder just incase someone has a console log in a script tag
                src: [
                    '<%= yeoman.app %>/scripts/app.js',
                    '<%= yeoman.app %>/scripts/bower-overrides.js',
                    '<%= yeoman.app %>/scripts/*/**.js',
                    '<%= yeoman.app %>/views/**/*.html'],
                options: {
                    str: 'console\\.log',
                    fail: true
                }
            }
        },
        replace: {
            dist: {
                src: ['<%= yeoman.dist %>/views/**/*.html'],
                overwrite: true,
                replacements: [
                    {
                        // replace all {{ }} bindins that don't come after a ' or ", repace with the start of a span
                        // (>[^<>]*) matches the closing of an html tag, so we know we're not in an html attribute, plus
                        // any characters up to the binding open {{.
                        // {{[ ]* matches the binding open {{ and possible spaces until the binding variable name
                        // ([\w|\$|\.]*) matches the variable name including dollar signs just in case its angular owned
                        // [ ]*}} matches white space to the binding close }}
                        // ([^<>]*<) matches any characters up to an open html tag <, making sure we're not in an attribute
                        from: /(>[^<>]*){{[ ]*([\w|\$|\.]*)[ ]*}}([^<>]*<)/gm,
                        to: '$1<span ng-bind="$2"></span>$3'
                    }
                ]
            }
        },
        // need two separate tasks here (-dev and -dist) because I can't change the base from app to dist without a new task
        html2js: {
            options: {
                module: 'templates',
                rename: function (moduleName) {
                    return '/' + moduleName;
                }
            },
            dev: {
                options: {
                    base: 'app'
                },
                src: ['app/views/**/*.html'],
                dest: 'app/scripts/templates.js'
            },
            dist: {
                options: {
                    base: 'dist'
                },
                src: ['dist/views/**/*.html'],
                dest: 'dist/scripts/templates.js'
            }
        },
        less: {
            dev: {
                options: {
                    paths: ['<%= yeoman.app %>/less']
                },
                files: {
                    '<%= yeoman.app %>/styles/entrypoint.css': '<%= yeoman.app %>/less/entrypoint.less'
                }
            }
        },
        preprocess: {
            inline: {
                src: ['<%= yeoman.dist %>/*.html'],
                options: {
                    inline: true
                }
            }
        },
        watch: {
            //copy_fonts: {
            //    files: ['<%= yeoman.app %>/bower_components/bootstrap/fonts/{,*/}*.*', '<%= yeoman.app %>/bower_components/elusive-iconfont/fonts/{,*/}*.*' ],
            //    tasks: ['copy:dev_fonts']
            //},
            //copy_kendo_images: {
            //    files: ['<%= yeoman.app %>/bowerless_components/kendo-ui/styles/Uniform/{,*/}*.*'],
            //    tasks: ['copy:kendo_images']
            //},
            html2js: {
                files: ['<%= yeoman.app %>/views/**/*.html'],
                tasks: ['html2js:dev']
            },
            coffee: {
                files: ['<%= yeoman.app %>/scripts/{,*/}*.coffee'],
                tasks: ['coffee:dist']
            },
            coffeeTest: {
                files: ['test/spec/{,*/}*.coffee'],
                tasks: ['coffee:test']
            },
            less: {
                files: ['<%= yeoman.app %>/less/{,*/}*.less'],
                tasks: ['less:dev']
            },
            livereload: {
                options: {
                    livereload: LIVERELOAD_PORT
                },
                files: [
                    '{.tmp,<%= yeoman.app %>}/styles/{,*/}*.css',
                    '{.tmp,<%= yeoman.app %>}/scripts/{,*/}{,*/}*.js',
                    '{.t yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        connect: {
            options: {
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: '0.0.0.0'
            },
            dev: {
                options: {
                    port: DEV_PORT,
                    middleware: function (connect) {
                        return [
                            lrSnippet,
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, yeomanConfig.app)
                        ];
                    }
                }
            },
            staticFiles: {
                options: {
                    protocol: 'https',
                    port: 443,
                    debug: true,
                    key: grunt.file.read('certs/server.key').toString(),
                    cert: grunt.file.read('certs/server.cer').toString(),
                    ca: grunt.file.read('certs/ca.cer').toString(),
                    passphrase: 'grunt',
                    middleware: function (connect) {
                        return [
                            lrSnippet,
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, yeomanConfig.app)
                        ];
                    }
                }
            },
            test: {
                options: {
                    // override the port so we can run karma testing while a liveloading server is running too
                    port: TEST_PORT,
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, 'test')
                        ];
                    }
                }
            },
            dist: {
                options: {
                    port: DIST_PORT,
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, yeomanConfig.dist)
                        ];
                    }
                }
            }
        },
        open: {
            dev: {
                url: 'http://localhost:' + DEV_PORT
            },
            dist: {
                url: 'http://localhost:' + DIST_PORT
            }
        },
        clean: {
            dist: {
                files: [
                    {
                        dot: true,
                        src: [
                            '.tmp',
                            '<%= yeoman.dist %>/*',
                            '!<%= yeoman.dist %>/.git*'
                        ]
                    }
                ]
            },
            server: '.tmp'
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                '<%= yeoman.app %>/scripts/{,*/}*.js'
            ]
        },
        coffee: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/scripts',
                        src: '{,*/}*.coffee',
                        dest: '.tmp/scripts',
                        ext: '.js'
                    }
                ]
            },
            dev: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/scripts',
                        src: '{,*/}*.coffee',
                        dest: '',
                        ext: '.js'
                    }
                ]
            },
            test: {
                files: [
                    {
                        expand: true,
                        cwd: 'test/spec',
                        src: '{,*/}*.coffee',
                        dest: '.tmp/spec',
                        ext: '.js'
                    }
                ]
            }
        },
        // not used since Uglify task does concat,
        // but still available if needed
        /*concat: {
         dist: {}
         },*/
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= yeoman.dist %>/scripts/{,*/}*.js',
                        '<%= yeoman.dist %>/styles/{,*/}*.css',
                        '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                        '<%= yeoman.dist %>/styles/fonts/*'
                    ]
                }
            }
        },
        useminPrepare: {
            html: ['<%= yeoman.app %>/*.html'],
            options: {
                dest: '<%= yeoman.dist %>'
            }
        },
        usemin: {
            html: ['<%= yeoman.dist %>/*.html', '<%= yeoman.dist %>/views/**/*.html'],
            css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
            options: {
                dirs: ['<%= yeoman.dist %>']
            }
        },
        imagemin: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/images',
                        //src: '{,*/}*.{png,jpg,jpeg}',
                        src: '{,*/}*.*',
                        dest: '<%= yeoman.dist %>/images'
                    }
                ]
            }
        },
        cssmin: {
            // By default, your `index.html` <!-- Usemin Block --> will take care of
            // minification. This option is pre-configured if you do not wish to use
            // Usemin blocks.
            // dist: {
            //   files: {
            //     '<%= yeoman.dist %>/styles/main.css': [
            //       '.tmp/styles/{,*/}*.css',
            //       '<%= yeoman.app %>/styles/{,*/}*.css'
            //     ]
            //   }
            // }
        },
        htmlmin: {
            dist: {
                options: {
                    /*removeCommentsFromCDATA: true,
                     // https://github.com/yeoman/grunt-usemin/issues/44
                     //collapseWhitespace: true,
                     collapseBooleanAttributes: true,
                     removeAttributeQuotes: true,
                     removeRedundantAttributes: true,
                     useShortDoctype: true,
                     removeEmptyAttributes: true,
                     removeOptionalTags: true*/
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>',
                        src: ['*.html', 'views/**/*.html'],
                        dest: '<%= yeoman.dist %>'
                    }
                ]
            }
        },
        // Put files not handled in other tasks here
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: false,
                        cwd: '<%= yeoman.app %>',
                        dest: '<%= yeoman.dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            '.htaccess',
                            'config/*.*',
                            'fonts/*.*',
                            'images/{,*/}*.{gif,webp,svg}',
                            'styles/fonts/*',
                            'bowerless_components/kendo-ui/styles/Uniform/*.*',
                            'styles/Uniform/*.*',
                            'styles/images/*.*'
                        ]
                    },
                    {
                        expand: true,
                        cwd: '.tmp/images',
                        dest: '<%= yeoman.dist %>/images',
                        src: [
                            'generated/*'
                        ]
                    }
                ]
            },
            dev_fonts: {
                files: [
                    {
                        expand: true,
                        flatten: true, // Go only to the last level (copy the files only)
                        dot: true,
                        cwd: '<%= yeoman.app %>/bower_components',
                        dest: '<%= yeoman.app %>/fonts',
                        src: [
                            'bootstrap/fonts/*.*',
                            'elusive-iconfont/fonts/*.*'
                        ]
                    }
                ]
            },
            kendo_images: {
                files: [
                    {
                        expand: true,
                        flatten: true, // Go only to the last level (copy the files only)
                        dot: true,
                        cwd: '<%= yeoman.app %>/bowerless_components',
                        dest: '<%= yeoman.app %>/styles/Uniform',
                        src: [
                            'kendo-ui/styles/Uniform/*.*'
                        ]
                    }
                ]
            },
            jquery_ui_images: {
                files: [
                    {
                        expand: true,
                        flatten: true, // Go only to the last level (copy the files only)
                        dot: true,
                        cwd: '<%= yeoman.app %>/bower_components',
                        dest: '<%= yeoman.app %>/styles/images',
                        src: [
                            'jquery-ui/themes/overcast/images/*.*'
                        ]
                    }
                ]
            },
            // copy bower components not handled in build
            bower_components: {
                files: [
                    {
                        expand: true,
                        flatten: false, // Go only to the last level (copy the files only)
                        dot: true,
                        cwd: '<%= yeoman.app %>',
                        dest: '<%= yeoman.dist %>',
                        src: [
                            //'bowerless_components/kendo-ui/js/kendo.web.min.js',
                            //'bowerless_components/kendo-ui/styles/kendo.uniform.min.css',
                            //'bowerless_components/kendo-ui/styles/kendo.common.min.css',
                            'bowerless_components/**/*',
                            'bower_components/es5-shim/es5-shim.js',
                            'bower_components/json3/lib/json3.min.js',
                            'bower_components/respond/respond.min.js',
                            'bower_components/respond/cross-domain/*'
                        ]
                    }
                ]
            },
            views: {
                files: [
                    {
                        expand: true,
                        flatten: false,
                        dot: true,
                        cwd: '<%= yeoman.app %>',
                        dest: '<%= yeoman.dist %>',
                        src: [
                            '*.html',
                            'views/**/*.html'
                        ]
                    }
                ]
            }
        },
        concurrent: {
            server: [
                'coffee:dist',
                'less',
                'html2js:dev'
            ],
            test: [
                'coffee',
                'less'
            ],
            dist: [
                'coffee',
                'less',
                'imagemin',
                'htmlmin'
                //'copy:views',
            ]
        },
        karma: {
            e2e: {
                configFile: 'karma-e2e.conf.js'
            },
            unit: {
                configFile: 'karma.conf.js',
                singleRun: true,
                browsers: ['PhantomJS']
            },
            watch: {
                configFile: 'karma.conf.js',
                autoWatch: true,
                browsers: ['PhantomJS']
            }
        },
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/*.html']
            }
        },
        ngmin: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.dist %>/scripts',
                        src: ['*.js', '!templates.js'],
                        dest: '<%= yeoman.dist %>/scripts'
                    }
                ]
            },
            templates: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.dist %>/scripts',
                        src: ['templates.js'],
                        dest: '<%= yeoman.dist %>/scripts'
                    }
                ]
            }

        },
        uglify: {
            dist: {
                files: {
                    // build comment blocks in index.html automatically uglify
                }
            }
        }
    });

    grunt.registerTask('server', function (target) {
        if (target === 'dist') {
            return grunt.task.run([
                //'build', // don't build here, we'll manually build and open up this server for testing
                'open:dist',
                'connect:dist:keepalive'
            ]);
        } else if (target === 'static') {
            return grunt.task.run([
                'clean:server',
                'concurrent:server',
                'connect:staticFiles', // host static files over https, if host file points local host to alpha, we can instantly test local files in ie8
                'copy:dev_fonts',
                'copy:kendo_images',
                'copy:jquery_ui_images',
                'open:dev',
                'watch'
            ]);
        } else {
            return grunt.task.run([
                'clean:server',
                'concurrent:server',
                'connect:dev',
                'copy:dev_fonts',
                'copy:kendo_images',
                'copy:jquery_ui_images',
                'open:dev',
                'watch'
            ]);
        }
    });

    grunt.registerTask('test', [
        'clean:server',
        'concurrent:test',
        'connect:test',
        'env',
        'karma:unit'
    ]);

    grunt.registerTask('test:e2e', [
        'clean:server',
        'coffee',
        'compass',
        'livereload-start',
        'connect:livereload',
        'karma:e2e'
    ]);

    grunt.registerTask('testing', [
        'clean:server',
        'concurrent:test',
        'connect:test',
        'env',
        'karma:watch'
    ]);

    grunt.registerTask('build', [
        'match',
        'clean:dist',
        'useminPrepare',
        //'concurrent:dist',

        'coffee',
        'less',
        'imagemin',
        'htmlmin',

        'concat',
        'copy:dev_fonts',
        'copy:kendo_images',
        'copy:jquery_ui_images',
        'copy:bower_components',
        'copy:dist',
        //'preprocess:inline',
        'replace:dist',
        'cdnify',
        'ngmin:dist',
        'cssmin',
        'uglify',
        //'rev', Rev would add hashes to filenames. No longer required.
        'usemin',
        // build templates.js last so it can generate from minified html
        'html2js:dist',
        'ngmin:templates'
    ]);

    grunt.registerTask('default', [
        'jshint',
        'env',
        'test',
        'build'
    ]);

    grunt.registerTask('deploy', function () {
        // Deployment servers, environments, init questions and init tasks here
        var appName         = 'ep',
            deployServers   = [
                {name: 'ux1', host: '10.10.1.81'},
                {name: 'ux2', host: '10.10.11.22'}
            ],
            environments    = [ // These will be used to build user names in the format {{env}}_ux1, eg dev_ux1
                {name: 'dev', color: 'green'},
                {name: 'stg', color: 'cyan'},
                {name: 'sandbox', color: 'rainbow'},
                {name: 'prod', color: 'red'},
                {name: 'qa', color: 'white'}
            ],
            sftpFiles       = { // Files for transfer
                './': 'dist/**/*'
            },
            sftpDefaults    = { // Options for the file transfer
                showProgress: true,
                createDirectories: true,
                srcBasePath: 'dist/'
            },
            prePromptTasks  = [ // Tasks to run before the file transfer
            ],
            promptTasks     = ['prompt:deploySftp'],
            postDeployTasks = [ // Tasks to be run after the file transfer
            ],
            questions       = [{ // Ask these questions before asking for passwords
                config: 'deployEnv',
                type: 'list',
                message: 'To which environment are you deploying?'.yellow,
                default: {name: 'dev'},
                choices: function () {
                    return _.map(environments, function (user) {
                        return {name: user.name[user.color], value: user.name};
                    });
                }
            }, {
                config: 'deployFolder',
                type: 'input',
                message: 'What should be the name of the deployment folder?'.magenta,
                default: moment().format('YYYY_MM_DD_HH_mm')
            }, {
                config: 'deployPassword',
                type: 'password',
                message: 'Password for chosen environment'.cyan
            }],
            processPrompts  = function (results) {
                // Build deployment Tasks
                var deploymentTasks = _.chain(deployServers).map(function (server) {
                        var userName       = util.format('%s_ux1', results.deployEnv),
                            deployPath     = util.format('/home/%s/apps/%s/%s', userName, appName, results.deployFolder),
                        // SFTP Task Config
                            sftpTask       = util.format('sftp:deploy_%s', server.name),
                            sftpConfig     = util.format('sftp.deploy_%s', server.name),
                            sftpOptions    = {
                                files: sftpFiles,
                                options: _.defaults({
                                    host: server.host,
                                    path: deployPath,
                                    username: userName,
                                    password: results.deployPassword
                                }, sftpDefaults)
                            },
                        // Notify Task Config
                            notifyOptions  = util.format('notify.deploy_%s', server.name),
                            notifyTask     = util.format('notify:deploy_%s', server.name),
                            notifyStart    = {title: util.format('Deployment Starting for %s', server.name)},
                            notifyComplete = {title: util.format('Deployment Complete for %s', server.name)},
                            notifyMsg      = {
                                message: _.chain(sftpOptions.options).omit('password').
                                    reduce(function (msg, val, key) {
                                        return msg + util.format('%s: %s\n', key, val);
                                    }, '').value()
                            };

                        // Setup sftp task
                        grunt.config(sftpConfig, sftpOptions);

                        // Set up Notify Tasks
                        grunt.config(notifyOptions + '_start.options', _.defaults(notifyStart, notifyMsg));
                        grunt.config(notifyOptions + '_done.options', _.defaults(notifyComplete, notifyMsg));

                        // If password is blank, skip deployment to that server
                        if (_.isEmpty(results.deployPassword)) {
                            console.log(util.format('Skipping server: %s, no password provided').red, server.name);
                            return [];
                        }

                        // Add the sftp and notify to the task queue
                        return [
                                notifyTask + '_start',
                            sftpTask,
                                notifyTask + '_done'
                        ];

                    }).flatten().value(),
                    postPromptTasks = deploymentTasks.concat(postDeployTasks);

                // Queue up deployment and post deployment tasks
                grunt.task.run(postPromptTasks);
            };

        // Add Questions and Then to prompt:deploySftp task
        grunt.config('prompt.deploySftp.options', {
            questions: questions,
            then: processPrompts
        });

        // Run the init tasks
        grunt.task.run(prePromptTasks.concat(promptTasks));
    });
};