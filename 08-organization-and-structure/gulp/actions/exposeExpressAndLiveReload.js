var connectLr           = require('connect-livereload'),
    express             = require('express'),
    app                 = express(),
    lrServer          = require('tiny-lr')();

module.exports = {

   notifyLiveReload:  function(callback) {
    lrServer.changed({ body: { files: ['index.html'] } });
    if(callback) callback();
   },

   startExpress : function(buildProperties, callback) {
       app.use(connectLr());
       app.use(express.static(buildProperties.expressRoot));
       app.listen(buildProperties.expressPort);
       console.log('Server running at http://localhost:'+buildProperties.expressPort);
       lrServer.listen(buildProperties.livereloadPort, function(err) {
           if (err) {
               return console.log(err);
           }
       });
   }
};