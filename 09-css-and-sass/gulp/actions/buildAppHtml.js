var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

module.exports = function(buildProperties, callback) {
    return gulp.src(buildProperties.appFiles.htmlFiles)
        .pipe(gulp.dest(buildProperties.expressRoot))
        .pipe(plugins.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(gulp.dest(buildProperties.publicDir));
};
