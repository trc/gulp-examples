function pipeSomeCssThroughGulp(callback) {
    gulp.src(['./css/file-one.css', './style/**/*'])
        .pipe(plugins.concat('./css/app.css'))
        .pipe(streamify(plugins.size({ showFiles: true })))
        .pipe(gulp.dest('./local-server/www/css'))
        .pipe(plugins.minifyCss())
        .pipe(streamify(plugins.size({ showFiles: true })))
        .pipe(streamify(plugins.rev()))
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist/css'))
        .on('error', plugins.util.log)
        .on('end', function () {
            plugins.util.log('Done building app.css...');
            if (callback) callback();
        });
}
