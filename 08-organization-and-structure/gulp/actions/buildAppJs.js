var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

module.exports = function(buildProperties, callback){
    return gulp.src(buildProperties.appFiles.jsFiles)
        .pipe(plugins.concat('js/app.js'))
        .pipe(gulp.dest(buildProperties.expressRoot))
        .pipe(plugins.streamify(plugins.uglify({ mangle: false })))
        .pipe(plugins.rev())
        .pipe(plugins.rename({ suffix: 'min' }))
        .pipe(gulp.dest(buildProperties.publicDir));
};
