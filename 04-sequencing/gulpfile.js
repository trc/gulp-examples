'use strict';

var doSomething         = require('./do-something'),
    gulp                = require('gulp'),
    runSequence         = require('run-sequence');

gulp.task('default', ['do-something-01'], function(callback) {

    runSequence('do-something-03', ['do-something-04', 'do-something-05'], 'do-something-06', function()
    {
        console.log("\r\n\r\n\t\tHello from Gulp!\r\n");
        console.log("\r\n\t\tLook at my beautiful concurrently-running and properly-ordered tasks!\r\n\r\n");
    });
});

gulp.task('do-something-01', function(callback) {
   doSomething(1, 250, callback);
});

gulp.task('do-something-02', function(callback) {
    doSomething(2, 300, callback);
});

gulp.task('do-something-03', ['do-something-02'], function(callback) {
    doSomething(3, 550, callback);
});

gulp.task('do-something-04', function(callback) {
    doSomething(4, 775, callback);
});

gulp.task('do-something-05', function(callback) {
    doSomething(5, 385, callback);
});

gulp.task('do-something-06', function(callback) {
    doSomething(6, 250, callback);
});