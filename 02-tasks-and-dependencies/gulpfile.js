'use strict';

var gulp                = require('gulp');

gulp.task('default', ['do-something-01', 'do-something-03'], function(callback) {

    console.log("\r\n\r\n\t\tHello from Gulp!\r\n\r\n");

});

gulp.task('do-something-01', function(callback) {
   console.log("\t\t\tdo-something-01 doing something...");
   if(callback) callback();
});

gulp.task('do-something-02', function(callback) {
    console.log("\t\t\tdo-something-02 doing something...");
    //Comment out to show that callbacks need to be called to trigger task end.
    if(callback) callback();
});

gulp.task('do-something-03', ['do-something-02'], function(callback) {
    console.log("\t\t\tdo-something-03 doing something...");
    if(callback) callback();
});