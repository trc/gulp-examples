'use strict';

var gulp                = require('gulp'),
    plugins             = require('gulp-load-plugins')({ lazy: true}),
    runSequence         = require('run-sequence');

var actions = {
        buildAppCss            : require('./gulp/actions/buildAppCss'),
        buildAppHtml           : require('./gulp/actions/buildAppHtml'),
        buildAppJs             : require('./gulp/actions/buildAppJs'),
        buildVendorCss         : require('./gulp/actions/buildVendorCss'),
        buildVendorJs          : require('./gulp/actions/buildVendorJs'),
        clean                  : require('./gulp/actions/clean'),
        copyAssets             : require('./gulp/actions/copyAssets'),
        startExpress           : require('./gulp/actions/exposeExpressAndLiveReload').startExpress,
        notifyLiveReload       : require('./gulp/actions/exposeExpressAndLiveReload').notifyLiveReload
};

var buildProperties = {
    appFiles            : require('./gulp/src-lists/app-files'),
    expressPort         : 4008,
    expressRoot         : require('path').resolve('./build/express-tmp'),
    livereloadPort      : 35729,
    publicDir           : require('path').resolve('./build/www'),
    vendorFiles         : require('./gulp/src-lists/vendor-files')
};

gulp.task('build-app-html', function(callback) { return actions.buildAppHtml(buildProperties, callback); });
gulp.task('build-app-js', function(callback) { return actions.buildAppJs(buildProperties, callback); });
gulp.task('build-vendor-js', function(callback) { return actions.buildVendorJs(buildProperties, callback); });
gulp.task('clean', actions.clean);
gulp.task('copy-assets', function(callback) { return actions.copyAssets(buildProperties, callback); });
gulp.task('notifyReload', actions.notifyLiveReload);
gulp.task('run-express', function(callback) { return actions.startExpress(buildProperties, callback); });

gulp.task('default', function(callback) {

    runSequence('clean', 'build-vendor-js', 'build-app-js', ['copy-assets', 'build-app-html'], 'run-express', function()
    {
        console.log("\r\n\r\n\t\tHello from Gulp!\r\n");
        console.log("\r\n\t\tWe're now serving up an AngularJS application on localhost:" + buildProperties.expressPort + "...\r\n\r\n");
    });

    gulp.watch('app/index.html', function() { runSequence('build-app-html', 'notifyReload'); });
    gulp.watch('app/assets/*', function(){ runSequence('copy-assets', 'notifyReload'); });
    gulp.watch('app/assets/**/*', function(){ runSequence('copy-assets', 'notifyReload'); });
    gulp.watch('app/js/*.js', function(){ runSequence('build-app-js', 'notifyReload'); });
});





