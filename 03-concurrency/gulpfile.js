'use strict';

var gulp                = require('gulp');

gulp.task('default', ['do-something-01', 'do-something-03', 'do-something-04'], function(callback) {

    console.log("\r\n\r\n\t\tHello from Gulp!\r\n\r\n");
    console.log("\r\n\r\n\t\tLook at my beautiful concurrently-running tasks!\r\n\r\n");

});

gulp.task('do-something-01', function(callback) {
   console.log("\t\t\tdo-something-01 doing something...");
   var iterations = 0;
    var interval = setInterval(function() {
       iterations++;
        if(iterations >= 10)
        {
            clearInterval(interval);
            if(callback) callback();
        }
        else
        {
            console.log("\t\t\tdo-something-01 doing something...");
        }
    }, 1000);
});

gulp.task('do-something-02', function(callback) {
    console.log("\t\t\tdo-something-02 doing something...");
    var iterations = 0;
    var interval = setInterval(function() {
        iterations++;
        if(iterations >= 10)
        {
            clearInterval(interval);
            if(callback) callback();
        }
        else
        {
            console.log("\t\t\tdo-something-02 doing something...");
        }
    }, 1500);
});

gulp.task('do-something-03', ['do-something-02'], function(callback) {
    console.log("\t\t\tdo-something-03 doing something...");
    var iterations = 0;
    var interval = setInterval(function() {
        iterations++;
        if(iterations >= 10)
        {
            clearInterval(interval);
            if(callback) callback();
        }
        else
        {
            console.log("\t\t\tdo-something-03 doing something...");
        }
    }, 1000);
});

gulp.task('do-something-04', function(callback) {
    console.log("\t\t\tdo-something-04 doing something...");
    var iterations = 0;
    var interval = setInterval(function() {
        iterations++;
        if(iterations >= 15)
        {
            clearInterval(interval);
            if(callback) callback();
        }
        else
        {
            console.log("\t\t\tdo-something-04 doing something...");
        }
    }, 630);
});